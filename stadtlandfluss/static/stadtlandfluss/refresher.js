// key function for client side page updating (polling)
// (java-script and html can be manipulated but concept prevents cheating)
function getQuery() {
  let request = new XMLHttpRequest();
  let method = 'GET';

  let url = window.location.href;
  request.open(method, url);
  request.onload = function () {
    let sendHTML = request.response;

    // ----------------- UPDATABLE BLOCK ----------------------------------------
    // only important page content will be updated
    let doc = new DOMParser().parseFromString(sendHTML, "text/html");

    updatables_len = updatables.length;
    if (updatables_len > 0) {
        let updatablesHTML = doc.getElementsByClassName("updatable");
        if (updatables_len == updatablesHTML.length) {
            for (let i = 0; i < updatables_len; i++) {
                updatables[i].innerHTML = updatablesHTML[i].innerHTML;
            }
        }
    }

    // ----------------- COUNTDOWN/AUTOSUBMIT ----------------------------------------
    if (form != null && countdown != null) {
        // local counter update (but autosubmit is forced by server and can't be manipulated)
        count--;
        countdown.innerHTML = count.toString(10);

        // autosubmit id requested by server
        if (doc.getElementsByClassName("autosubmit-requester")[0] != null) {
            form.submit();
            document.documentElement.innerHTML = sendHTML;
        }
    }
  };
  request.send();
}

let updatables = document.getElementsByClassName("updatable");

// just one countdown html block on each page allowed
let form = document.getElementsByClassName("autosubmit-form")[0];
let countdown = document.getElementsByClassName("countdown")[0];
let count = null
if (countdown != null) {
    // countdown start value extracted from html
    count = parseInt(countdown.innerHTML, 10);
}

// poll each second (balance of efficiency and user experience)
setInterval(getQuery, 1000);
