// copy text to clipboard
function copyText(text) {
  navigator.clipboard.writeText(text);
}

// copy invitation link to clipboard
function copyInvite(page, game_id) {
    invite_text = 'Want to play Stadt Land Fluss with me? Click following link to join the game: https://'
    copyText(invite_text + window.location.host + page + '?lobby_id=' + game_id);
}
