import uuid

from django import forms
from django.core.validators import MaxValueValidator, MinValueValidator


class CreateGameForm(forms.Form):
    username = forms.CharField(label='Your name', max_length=100)
    rounds = forms.IntegerField(label="Number of Rounds to play",
                                validators=[MaxValueValidator(26), MinValueValidator(1)])
    round_max_length = forms.IntegerField(validators=[MaxValueValidator(300), MinValueValidator(5)], label="Max. length of a round (seconds)")


class JoinGameForm(forms.Form):
    username = forms.CharField(label='Your name', max_length=100)
    lobby_id = forms.UUIDField(label='Lobby ID')


class AddCategoryForm(forms.Form):
    new_category_name = forms.CharField(label='Add Category', max_length=100)


class PlayForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.cat_names = kwargs.pop('cat_names')
        super(PlayForm, self).__init__(*args, **kwargs)
        for cat_name in self.cat_names:
            self.fields[cat_name] = forms.CharField(label=cat_name, max_length=100, required=False)


class CustomRadioSelect(forms.RadioSelect):
    option_template_name = "widgets/custom_radio_select.html"


class VoteForm(forms.Form):

    def __init__(self, *args, **kwargs):
        CHOICES = [(0, '0'),
                   (5, '5'),
                   (10, '10'),
                   (20, '20')]

        self.cat_names = kwargs.pop('cat_names')
        super(VoteForm, self).__init__(*args, **kwargs)
        for cat_name in self.cat_names:
            self.fields[cat_name] = forms.ChoiceField(choices=CHOICES, widget=CustomRadioSelect, initial=0)

