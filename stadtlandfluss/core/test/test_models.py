import uuid

from django.db.utils import IntegrityError
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.test import TestCase
from core.models import GameSession, PlayerResultHandler, Round, PlayerAnswersPerRound


class GameSessionTests(TestCase):
    def setUp(self) -> None:
        """
        Setup two Users and one GameSession for the following test cases
        """
        User.objects.create(username='tester', password="123")
        User.objects.create(username='tester2', password="123")
        self.test_game_1 = GameSession.objects.create(
            admin=User.objects.get(username='tester2'),
            categories=["Land"]
        )

    def test_gamesession_creation_ok(self):
        """
        Testing if the creation of a game happens as it should.
        Testing if all default values are correct.
        """
        game = GameSession.objects.create(admin=User.objects.get(username='tester'), categories=["Stadt"])
        game.full_clean()
        game.save()
        self.assertEqual(game.admin, User.objects.get(username='tester'))
        self.assertEqual(game.categories, ["Stadt"])
        self.assertEqual(len(game.categories), 1)
        self.assertTrue(isinstance(game.lobby_id, uuid.UUID))
        self.assertEqual(game.rounds, 5)
        self.assertEqual(game.round_max_length, 20)
        self.assertEqual(len(game.letters), 26)

    def test_gamesession_creation_not_ok(self):
        """
        Testing if the creating of a false GameSession is forbidden.
        """
        game = GameSession(round_max_length=301, rounds=27)
        with self.assertRaises(ValidationError) as ve:
            game.full_clean()
        # admin
        self.assertEqual(ve.exception.messages[0], 'This field cannot be null.')
        # rounds
        self.assertEqual(ve.exception.messages[1], 'Ensure this value is less than or equal to 26.')
        # round_max_length
        self.assertEqual(ve.exception.messages[2], 'Ensure this value is less than or equal to 300.')
        # categories
        self.assertEqual(ve.exception.messages[3], 'This field cannot be blank.')
        # Object in the database cannot be created without admin
        self.assertRaises(IntegrityError, GameSession.objects.create)

    def test_gamesession_get_from_db(self):
        """
        Testing if the existing GameSession from the Setup can be gotten.
        """
        expected_game = GameSession.objects.get(lobby_id=self.test_game_1.lobby_id)
        self.assertEqual(expected_game.admin, User.objects.get(username='tester2'))
        self.assertEqual(expected_game.categories, ["Land"])
        self.assertEqual(len(expected_game.categories), 1)
        self.assertTrue(isinstance(expected_game.lobby_id, uuid.UUID))
        self.assertEqual(expected_game.rounds, 5)
        self.assertEqual(expected_game.round_max_length, 20)

    def test_gamesession_deletion(self):
        """
        Testing if the deletion of a GameSession object works.
        """
        game = GameSession.objects.create(admin=User.objects.get(username='tester'))
        self.assertTrue(GameSession.objects.filter(lobby_id=game.lobby_id).exists())
        game.delete()
        self.assertFalse(GameSession.objects.filter(lobby_id=game.lobby_id).exists())

    def test_multiple_gamesessions_one_user(self):
        """
        Testing if a user can belong to more than one GameSession object
        in the database.
        """
        GameSession.objects.create(admin=User.objects.get(username='tester2'))
        self.assertEqual(
            len(GameSession.objects.filter(admin=User.objects.get(username='tester2'))),
            2
        )

    def test_letter_pick(self):
        letter = self.test_game_1.get_a_letter()
        self.assertFalse(letter in self.test_game_1.letters)
        self.assertEqual(len(self.test_game_1.letters), 25)


class PlayerResultHandlerTests(TestCase):
    def setUp(self) -> None:
        """
        Setup two Users, one GameSession and
        one PlayerResultHandler for the following test cases
        """
        User.objects.create(username='tester_admin', password="123")
        User.objects.create(username='tester', password="123")
        self.test_game_1 = GameSession.objects.create(
            admin=User.objects.get(username='tester_admin'),
            categories=["Stadt", "Land"]
        )
        self.tester_handler = PlayerResultHandler.objects.create(
            player=User.objects.get(username='tester_admin'),
            game=self.test_game_1
        )

    def test_create_handlers_ok(self):
        """
        Testing if the creation of a new handler works well with the default values.
        """
        p_admin = self.tester_handler
        p_tester = PlayerResultHandler.objects.create(player=User.objects.get(username='tester'), game=self.test_game_1)

        self.assertEqual(p_admin.game, p_tester.game)
        self.assertEqual(p_admin.total_score, 0)
        self.assertEqual(p_admin.total_score, p_tester.total_score)
        self.assertEqual(p_admin.players_state, -1)
        self.assertEqual(p_admin.players_state, p_tester.players_state)

    def test_create_handlers_not_ok(self):
        """
        Testing if the creation and save of a Handler without
        player and game is forbidden.
        """
        handler = PlayerResultHandler()
        with self.assertRaises(ValidationError) as ve:
            handler.full_clean()
        # game
        self.assertEqual(ve.exception.messages[0], 'This field cannot be null.')
        # player
        self.assertEqual(ve.exception.messages[1], 'This field cannot be null.')
        # integrity error
        self.assertRaises(IntegrityError, GameSession.objects.create)

    def test_get_handler_from_db(self):
        """
        Testing if the existing PlayerResultHandler from the Setup can be gotten.
        """
        expected_handler = PlayerResultHandler.objects.get(game=self.test_game_1, player=User.objects.get(username='tester_admin'))
        self.assertEqual(expected_handler.player, User.objects.get(username='tester_admin'))
        self.assertEqual(expected_handler.player, User.objects.get(username='tester_admin'))
        self.assertEqual(expected_handler.total_score, 0)
        self.assertEqual(expected_handler.players_state, -1)

    def test_handler_deletion(self):
        """
        Testing if the deletion of a PlayerResultHandler object works.
        """
        p_tester = PlayerResultHandler.objects.create(player=User.objects.get(username='tester'), game=self.test_game_1)
        self.assertTrue(PlayerResultHandler.objects.filter(player=User.objects.get(username='tester'), game=self.test_game_1).exists())
        p_tester.delete()
        self.assertFalse(PlayerResultHandler.objects.filter(player=User.objects.get(username='tester'), game=self.test_game_1).exists())


class RoundTests(TestCase):
    def setUp(self) -> None:
        """
        Setting up a User, a game and two rounds for testing the Round object.
        """
        User.objects.create(username='tester_admin', password="123")
        self.test_game = GameSession.objects.create(
            admin=User.objects.get(username='tester_admin'),
            categories=["Stadt", "Land"]
        )
        self.round = Round.objects.create(
            game=self.test_game,
            letter=self.test_game.get_a_letter(),

        )
        self.round2 = Round.objects.create(
            game=self.test_game,
            letter=self.test_game.get_a_letter(),
        )

    def test_create_round_ok(self):
        """
        Testing if the creation of a round with a referenced game and letter
        works.
        """
        round = Round.objects.create(game=self.test_game, letter=self.test_game.get_a_letter())
        self.assertTrue(isinstance(round, Round))
        self.assertTrue(isinstance(round.round_id, uuid.UUID))
        self.assertFalse(round.letter in self.test_game.letters)
        self.assertFalse(round.started)
        self.assertFalse(round.finished)

    def test_create_round_not_ok(self):
        """
        Testing if creating a round without a letter and a referenced
        game is forbidden.
        """
        round = Round()
        with self.assertRaises(ValidationError) as ve:
            round.full_clean()
        # game
        self.assertEqual(ve.exception.messages[0], 'This field cannot be null.')
        # letter
        self.assertEqual(ve.exception.messages[1], 'This field cannot be blank.')
        # integrity error
        self.assertRaises(IntegrityError, Round.objects.create)

    def test_get_rounds_from_db(self):
        """
        Testing if getting multiple rounds from the db works.
        """
        expected_rounds = Round.objects.filter(game=self.test_game)
        self.assertEqual(len(expected_rounds), 2)
        self.assertFalse(expected_rounds[0] == expected_rounds[1])
        self.assertFalse(expected_rounds[0].letter == expected_rounds[1].letter)

    def test_round_deletion(self):
        """
        Testing if the deletion of a Round object works.
        """
        round = Round.objects.create(game=self.test_game)
        self.assertTrue(Round.objects.filter(round_id=round.round_id).exists())
        round.delete()
        self.assertFalse(Round.objects.filter(round_id=round.round_id).exists())


class PlayerAnswersPerRoundTests(TestCase):
    def setUp(self) -> None:
        """
        Setting two users, a game, a round and a test PlayerAnswersPerRound object.
        """
        self.admin= User.objects.create(username='tester_admin', password="123")
        User.objects.create(username='tester', password="123")
        self.test_game = GameSession.objects.create(
            admin=User.objects.get(username='tester_admin'),
            categories=["Stadt", "Land"]
        )
        self.round = Round.objects.create(
            game=self.test_game,
            letter=self.test_game.get_a_letter(),
        )
        self.papr = PlayerAnswersPerRound.objects.create(
            player=self.admin,
            game=self.test_game,
            round=self.round
        )

    def test_create_playeranswersperround_ok(self):
        """
        Testing if the correct creation of a PlayerAnswersPerRound object works.
        """
        papr = PlayerAnswersPerRound.objects.create(
            player=User.objects.get(username='tester'),
            game=self.test_game,
            round=self.round
        )
        self.assertTrue(isinstance(papr, PlayerAnswersPerRound))
        self.assertTrue(isinstance(papr.player_answers_id, uuid.UUID))
        self.assertEqual(papr.answers, {})
        self.assertEqual(papr.answer_score, 0)

    def test_create_playeranswersperround_not_ok(self):
        """
        Testing if creating a PlayerAnswersPerRound object without
        player, game and round reference is forbidden.
        """
        papr = PlayerAnswersPerRound()
        with self.assertRaises(ValidationError) as ve:
            papr.full_clean()
        # player
        self.assertEqual(ve.exception.messages[0], 'This field cannot be null.')
        # game
        self.assertEqual(ve.exception.messages[1], 'This field cannot be null.')
        # round
        self.assertEqual(ve.exception.messages[2], 'This field cannot be null.')
        # integrity error
        self.assertRaises(IntegrityError, PlayerAnswersPerRound.objects.create)

    def test_get_playeranswersperround_from_db(self):
        """
        Testing if an existing PlayerAnswersPerRound object can be gotten from the db.
        """
        expected_papr = PlayerAnswersPerRound.objects.get(player=self.admin)
        self.assertEqual(expected_papr.game, self.test_game)
        self.assertEqual(expected_papr.round, self.round)

    def test_playeranswersperround_deletion(self):
        """
        Testing if the deletion of a PlayerAnswersPerRound object works.
        """
        papr = PlayerAnswersPerRound.objects.create(
            game=self.test_game,
            player=User.objects.get(username="tester"),
            round=self.round
        )
        self.assertTrue(PlayerAnswersPerRound.objects.filter(player=User.objects.get(username="tester")).exists())
        papr.delete()
        self.assertFalse(PlayerAnswersPerRound.objects.filter(player=User.objects.get(username="tester")).exists())
