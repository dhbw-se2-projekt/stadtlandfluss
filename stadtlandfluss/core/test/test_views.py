from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from core.models import GameSession, PlayerResultHandler, PlayerAnswersPerRound, Round


"""
Todo (not done due to time problems):
Provide tests for: 
    - PlayView POST
    - VoteView
    - ResultsView
    - Kicking a user 
"""


class WelcomeTests(TestCase):
    """
    Tests for the Welcome page, the join_game and the create_game views.
    """
    def setUp(self) -> None:
        """
        Setting up a test player, data to create a game, a GameSession, and data to join a game.
        """
        self.test_gamer= User.objects.create(username="test_gamer", password="123")
        self.create_data = {
            "username": "test_creator",
            "rounds": 2,
            "round_max_length": 10,
        }
        self.game = GameSession.objects.create(
            admin=self.test_gamer,
            categories="Baum",

        )
        self.join_data = {
            "username": "test_joiner",
            "lobby_id": self.game.lobby_id
        }

    def test_welcome_page(self):
        """
        Tests if the welcome view is displayed properly.
        """
        response = self.client.get(reverse('core:welcome'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Join a Game")
        self.assertContains(response, "Create a Game")

    def test_create_a_game_get_ok(self):
        """
        Tests if the create_game view is displayed properly.
        """
        response = self.client.post(reverse('core:game.create'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Your name:")
        self.assertContains(response, "Number of Rounds to play:")
        self.assertContains(response, "Max. length of a round (seconds):")
        self.assertContains(response, "Create a Game")

    def test_create_a_game_post_ok(self):
        """
        Tests if the creation of a game works well when all needed data is provided.
        """
        response = self.client.post(reverse('core:game.create'), self.create_data)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(User.objects.filter(username="test_creator").exists())
        game = GameSession.objects.get(admin=User.objects.get(username="test_creator"))
        self.assertTrue(PlayerResultHandler.objects.filter(
            player=User.objects.get(username="test_creator"),
            game=game
            ).exists())
        self.assertEqual(game.admin.username, 'test_creator')
        self.assertEqual(game.rounds, 2)
        self.assertEqual(game.round_max_length, 10)
        self.assertRedirects(response, f"/{game.lobby_id}/lobby", status_code=302)

    def test_join_a_game_get_ok(self):
        """
        Tests if the join_game view is displayed properly.
        """
        response = self.client.get(reverse('core:game.join'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Your name:")
        self.assertContains(response, "Lobby ID:")
        self.assertContains(response, "Join")

    def test_join_a_game_post_ok(self):
        """
        Tests if joining a game works well when all needed data is provided.
        """
        response = self.client.post(reverse('core:game.join'), self.join_data)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(User.objects.filter(username="test_joiner").exists())
        game = GameSession.objects.get(lobby_id=self.game.lobby_id)
        self.assertEqual(game.admin.username, 'test_gamer')
        self.assertEqual(game.rounds, 5)
        self.assertEqual(game.round_max_length, 20)
        self.assertRedirects(response, f"/{game.lobby_id}/lobby", status_code=302)


class LobbyWaitViewTests(TestCase):
    """
    Tests for the Lobby View.
    """
    def setUp(self) -> None:
        """
        Setting up two players, a GameSession and two PlayerResultHandlers.
        There is also an assertion list provided, to make asserting different displays easier.
        """
        self.tester1 = User.objects.create_user(username="tester1", password="123")
        self.tester2 = User.objects.create_user(username="tester2", password="123")
        self.game = GameSession.objects.create(
            admin=self.tester1,
            categories=["Stadt", "Land", "Fluss"]
        )
        PlayerResultHandler.objects.create(
            game=self.game,
            player=self.tester1
        )
        PlayerResultHandler.objects.create(
            game=self.game,
            player=self.tester2
        )
        self.assertion_list = [
            "Players", "tester1", "tester2",
            "Categories", "Stadt", "Land", "Fluss",

        ]

    def test_lobby_view_admin_get_ok(self):
        """
        Tests if the lobby gets displayed right, when the user is the GameSession admin.
        """
        # login the admin to have access to the LobbyWaitView
        result = self.client.login(username='tester1', password='123')
        self.assertTrue(result)

        # get the response content
        response = self.client.get(reverse('core:lobby.wait', kwargs={"gameid": self.game.lobby_id}))
        self.assertEqual(response.status_code, 200)


        # Assertion what has to be displayed in the lobby
        for x in self.assertion_list:
            self.assertContains(response, x)

        # Admin based Assertion
        self.assertContains(response, "Start a Round")

    def test_admin_add_category_ok(self):
        """
        Tests if the admin can add categories to the game.
        """
        # login the admin to have access to the LobbyWaitView
        result = self.client.login(username='tester1', password='123')
        self.assertTrue(result)

        add_category_response = self.client.post(
            reverse('core:lobby.wait', kwargs={"gameid": self.game.lobby_id}),
            {"new_category_name": "Beruf", },
        )
        self.assertEqual(add_category_response.status_code, 302)

        response2 = self.client.get(reverse('core:lobby.wait', kwargs={"gameid": self.game.lobby_id}))
        self.assertEqual(response2.status_code, 200)
        self.assertContains(response2, "Beruf")

    def test_lobby_view_player_get_ok(self):
        """
        Tests if the lobby gets displayed right, when the user is a normal player.
        """
        # login the player to have access to the LobbyWaitView
        result = self.client.login(username='tester2', password='123')
        self.assertTrue(result)

        # get the response content
        response = self.client.get(reverse('core:lobby.wait', kwargs={"gameid": self.game.lobby_id}))
        self.assertEqual(response.status_code, 200)

        # Assertion what has to be displayed in the lobby
        for x in self.assertion_list:
            self.assertContains(response, x)

        # Player based Assertion
        self.assertContains(response, "Wait for the admin to start a round...")


class StartARoundTests(TestCase):
    """
    Tests if all necessary objects get created when a round is started.
    """
    def setUp(self) -> None:
        """
        Setting up all needed objects that a working lobby might have:
        Two users, a GameSession, two PlayerResultHandlers
        """
        self.tester1 = User.objects.create_user(username="tester1", password="123")
        self.tester2 = User.objects.create_user(username="tester2", password="123")
        self.game = GameSession.objects.create(
            admin=self.tester1,
            categories=["Stadt", "Land", "Fluss"]
        )
        PlayerResultHandler.objects.create(
            game=self.game,
            player=self.tester1
        )
        PlayerResultHandler.objects.create(
            game=self.game,
            player=self.tester2
        )

    def test_start_a_round_admin(self):
        """
        Tests if the starting of a round by the admin creates the needed round object and a
        PlayerAnswersPerRound object for each player.
        """
        # login the admin to have access to the LobbyWaitView
        result = self.client.login(username='tester1', password='123')
        self.assertTrue(result)

        response = self.client.get(
            reverse('core:lobby.start_round', kwargs={"gameid": self.game.lobby_id})
        )

        self.assertEqual(response.status_code, 302)
        round = Round.objects.get(game=self.game)
        round.save()
        self.assertEqual(len(PlayerAnswersPerRound.objects.filter(game=self.game)), 2)
        self.assertRedirects(response, f"/{round.round_id}/play", status_code=302)


class PlayViewTests(TestCase):
    """
    Tests if the PlayViews Work properly. These tests are not ready to rely on.
    """
    def setUp(self) -> None:
        """
        Setting up every object that might be needed for testing the PlayView:
        Two users, a GameSession, two PlayerResultHandlers, a Round, two PlayerAnswersPerRound objects,
        an assertion list, some answers to play.
        """
        self.tester1 = User.objects.create_user(username="tester1", password="123")
        self.tester2 = User.objects.create_user(username="tester2", password="123")
        self.game = GameSession.objects.create(
            admin=self.tester1,
            categories=["Stadt", "Land", "Fluss"]
        )
        PlayerResultHandler.objects.create(
            game=self.game,
            player=self.tester1
        )
        PlayerResultHandler.objects.create(
            game=self.game,
            player=self.tester2
        )
        self.round = Round.objects.create(
            game=self.game
        )
        PlayerAnswersPerRound.objects.create(
            game=self.game,
            player=self.tester1,
            round=self.round
        )
        PlayerAnswersPerRound.objects.create(
            game=self.game,
            player=self.tester2,
            round=self.round
        )
        self.assertion_list = [
            self.round.letter, "Stadt", "Land", "Fluss", "Submit"
        ]

        self.play_words = {
            "Stadt": "foo1",
            "Land" : "foo2",
            "Fluss": "foo3",
        }

    def test_play_view_get_ok(self):
        """
        Tests if the play view gets displayed right.
        It should have a field for each category.
        """
        # login the player to have access to the PlayView
        result = self.client.login(username='tester1', password='123')
        self.assertTrue(result)

        response = self.client.get(reverse('core:lobby.play', kwargs={"round": self.round.round_id}))
        self.assertEqual(response.status_code, 200)

        # Assertion what has to be displayed in the lobby
        for x in self.assertion_list:
            self.assertContains(response, x)
