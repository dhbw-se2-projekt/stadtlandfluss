import random
import sys
import uuid
import json

from django.conf import global_settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class GameSession(models.Model):
    """
    Holds all the informations about the Game Session
    """
    lobby_id = models.UUIDField(
        default=uuid.uuid4,
        unique=True,
        editable=True,
        primary_key=True,
        verbose_name="lobby_id",
        null=False,
    )
    admin = models.ForeignKey(
        global_settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="game_admin",
        null=False,
    )
    rounds = models.IntegerField(
        validators=[MaxValueValidator(26), MinValueValidator(1)],
        default=5,
        null=False,
    )
    round_max_length = models.IntegerField(
        validators=[MaxValueValidator(300), MinValueValidator(5)],
        default=20,
        verbose_name="Max. length of a round (seconds)",
        null=False,
    )
    letters = models.CharField(
        default="ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        max_length=26,
        verbose_name="Moegliche Buchstaben",
        null=False,
    )
    categories = models.JSONField(
        default=list
    )

    def __str__(self):
        return str(self.lobby_id)

    def get_all_player_handlers(self):
        return self.handler_belongs_to_game.all()

    def get_all_rounds(self):
        return self.round_belongs_to_game.all()

    def get_all_answers(self):
        return self.answers_belong_to_game.all()

    def get_a_letter(self):
        """
        Picks a random letter from the alphabet and deletes it out of the list
        :return: Letter
        """
        picked_letter = random.choice(self.letters)
        self.letters = self.letters.replace(str(picked_letter), "")
        return picked_letter


class PlayerResultHandler(models.Model):
    """
    Holds and manages informations about the users
    """
    game = models.ForeignKey(
        GameSession, on_delete=models.CASCADE, related_name="handler_belongs_to_game"
    )
    player = models.ForeignKey(
        global_settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="handler_belongs_to_player",
    )
    total_score = models.IntegerField(default=0, verbose_name="total score")
    players_state = models.IntegerField(default=-1, verbose_name="players state")    # 0=not voted, 1=voted, -1=dead

    def get_players_state_related(self):
        if self.players_state == 1:
            return "has voted"
        elif self.players_state == 0:
            return "has not voted"
        else:
            return "has not played yet"

    def __str__(self):
        return self.player.username


class Round(models.Model):
    """
    Keeps track of the current round
    """
    round_id = models.UUIDField(
        default=uuid.uuid4,
        unique=True,
        editable=True,
        primary_key=True,
        verbose_name="round_id",
    )
    game = models.ForeignKey(
        GameSession, on_delete=models.CASCADE, related_name="round_belongs_to_game"
    )
    letter = models.CharField(max_length=1, default="", verbose_name="Buchstabe", blank=False)
    started = models.BooleanField(default=False, verbose_name="Runde begonnen")
    finished = models.BooleanField(default=False, verbose_name="Runde beendet")

    def __str__(self):
        return str(self.round_id)


class PlayerAnswersPerRound(models.Model):
    """
    Manages answers of the players in each round
    """
    player_answers_id = models.UUIDField(
        default=uuid.uuid4,
        unique=True,
        editable=True,
        primary_key=True,
        verbose_name="player_answers_id",
    )
    player = models.ForeignKey(
        global_settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="answers_belong_to_player",
    )
    game = models.ForeignKey(
        GameSession, on_delete=models.CASCADE, related_name="answers_belong_to_game"
    )
    round = models.ForeignKey(
        Round, on_delete=models.CASCADE, related_name="answers_belong_to_round"
    )
    answers = models.JSONField(
        default = dict
    )
    answer_score = models.IntegerField(
        default=0, verbose_name="answer score"
    )

    def __str__(self):
        return self.player.username
