import uuid
from functools import partial
from threading import Timer

from django.contrib.auth import login
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, render, reverse
from django.template import RequestContext
from django.views import generic
from .forms import CreateGameForm, JoinGameForm, VoteForm, PlayForm, AddCategoryForm
from core.models import GameSession, PlayerResultHandler, PlayerAnswersPerRound, Round
from django.http import HttpResponseRedirect


TIMER = {}


def bad_request(request, exception):
    """
    Shortcut view to render the default 400 site
    """
    response = render('400.html',
                      RequestContext(request))
    response.status_code = 400
    return response


def permission_denied(request, exception):
    """
    Shortcut view to render the default 403 site
    """
    response = render('403.html',
                      RequestContext(request))
    response.status_code = 403
    return response


def not_found(request, exception):
    """
    Shortcut view to render the default 404 site.
    """
    response = render('404.html',
                      RequestContext(request))
    response.status_code = 404
    return response


def create_and_login_user(request, username=""):
    """
    Creates a new user when somebody joins the lobby / lobby gets created
    :return: User
    """
    if User.objects.filter(username=username):
        user = User.objects.get(username=username)
    else:
        user = User.objects.create(username=username, password="123")
    login(request, user)
    return user


def delete_user(request, **kwargs):
    """
    Deletes user when he's getting kicked
    :return: Redirect Admin back to Lobby-Wait_View after kicking
    """
    lobby_id = get_object_or_404(GameSession, lobby_id=kwargs["gameid"])
    if "user" in request.POST.keys() and User.objects.filter(username=request.POST['user']):
            selected_user = User.objects.get(username=request.POST['user'])
            selected_user.delete()
    return HttpResponseRedirect(reverse("core:lobby.wait", args=[lobby_id]))


def create_view(request):
    """
    View to create a new Game Lobby
    Aquires all the needed information from the user (Length of Round, Number of Rounds)
    Calls lobby-creation.html
    :return: Redirect User to Lobby-Wait_View as Admin of Lobby
    """
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = CreateGameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            username = form.cleaned_data.get("username")
            rounds = form.cleaned_data.get("rounds")
            round_max_length = form.cleaned_data.get("round_max_length")
            lobby_id = uuid.uuid4()
            user = create_and_login_user(request, username)
            GameSession.objects.create(
                lobby_id=lobby_id,
                admin=user,
                rounds=rounds,
                round_max_length=round_max_length,
            )
            PlayerResultHandler.objects.create(
                game=GameSession.objects.get(lobby_id=lobby_id),
                player=user,
                total_score=0,
            )
            return HttpResponseRedirect(reverse("core:lobby.wait", args=[lobby_id]))

    else:
        form = CreateGameForm()

    return render(request, "lobby-creation.html", {"form": form})


def join_view(request):
    """
    View to join an existing lobby
    Aquires username and lobby_id from user
    Calls lobby-join.html
    :return: Redirect User to Lobby-Wait_View of desired lobby
    """
    if request.method == "POST":
        form = JoinGameForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            lobby_id = form.cleaned_data.get("lobby_id")
            user = create_and_login_user(request, username)
            PlayerResultHandler.objects.create(
                game=GameSession.objects.get(lobby_id=lobby_id),
                player=user,
                total_score=0,
            )
            return HttpResponseRedirect(reverse("core:lobby.wait", args=[lobby_id]))
    elif request.method == "GET":
        lobby_id = request.GET.get('lobby_id', '')
        form = JoinGameForm(initial=dict(lobby_id=lobby_id))
    else:
        form = JoinGameForm()

    return render(request, "lobby-join.html", {"form": form})


def start_a_round(request, **kwargs):
    """
    Starts a new round when Lobby-Admin presses button
    :return: Redirect all players to Play_View
    """
    game = get_object_or_404(GameSession, lobby_id=kwargs["gameid"])
    letter = game.get_a_letter()
    game.save()
    round = Round.objects.create(game=game, letter=letter)

    for handler in PlayerResultHandler.objects.filter(game=game.lobby_id):
        handler.players_state = 0
        handler.save()
        PlayerAnswersPerRound.objects.create(
            player=handler.player, game=game, round=round
        )
    return HttpResponseRedirect(reverse("core:lobby.play", args=[round]))


def stop_round(round):
    """
    Stops round timer and set round.finished Var to TRUE
    """
    if not round.finished:
        global TIMER
        TIMER[round.round_id].cancel()
        del TIMER[round.round_id]
        round.finished = True
        round.save()


def play_view(request, **kwargs):
    """
    Starts Timer and wait for player to input their answers
    Finish the round when timer runs out or a players submits their answers
    Calls play.html
    :return: Redirect all players to Vote_View
    """
    round = get_object_or_404(Round, round_id=kwargs['round'])
    game = get_object_or_404(GameSession, lobby_id=round.game.lobby_id)

    if request.method == 'POST':
        form = PlayForm(request.POST, cat_names=game.categories)
        papr = PlayerAnswersPerRound.objects.get(game=game, player=request.user, round=round)
        if form.is_valid():
            for cat in game.categories:
                papr.answers[cat] = form.cleaned_data.get(cat)
            papr.save()
            stop_round(round)
            return HttpResponseRedirect(reverse("core:lobby.vote", args=[papr.pk]))
    else:
        if not round.started:
            global TIMER
            TIMER[round.round_id] = Timer(
                game.round_max_length, partial(stop_round, round=round)
            )
            round.started = True
            round.save()
            TIMER[round.round_id].start()
        form = PlayForm(request.GET, cat_names=game.categories)
    return render(request, 'play.html', {'form': form, 'game': game, 'round': round})


def vote_view(request, **kwargs):
    """
    Aquires the points from the users for their answers
    Updates the player_state to keep track who has voted already
    Calls vote.html
    :return: Redirect user to Lobby-Wait_View after submitting the points he gave himself
    """
    papr = get_object_or_404(PlayerAnswersPerRound, player_answers_id=kwargs["pk"])

    # get answers from other players and safe them if already existing
    other_paprs = PlayerAnswersPerRound.objects.filter(game=papr.game, round=papr.round).exclude(player=request.user)
    categories_answers_infos = {}
    for key in papr.answers.keys():
        categories_answers_infos[key] = [papr.answers[key], PrintableDict({op.player.username: op.answers[key] for op in
                                                                           other_paprs if key in op.answers})]

    prh = PlayerResultHandler.objects.get(game=papr.game, player=papr.player)

    if request.method == 'POST':
        form = VoteForm(request.POST, cat_names=papr.answers.keys())
        if form.is_valid():
            for cat in papr.answers.keys():
                papr.answer_score += int(form.cleaned_data.get(cat))
            prh.total_score += papr.answer_score
            prh.players_state = 1
            prh.save()
            papr.save()
            return HttpResponseRedirect(reverse("core:lobby.wait", args=[papr.game.lobby_id]))
    else:
        form = VoteForm(cat_names=papr.answers.keys())

    return render(request, 'vote.html', {'form': form, 'categories_answers_infos': list(categories_answers_infos.items())})


class PrintableDict:
    """
        Helper class for custom dict printing (provides new __str__)
        """
    def __init__(self, d):
        self.dict = d

    def __str__(self):
        string = ""
        for key in self.dict.keys():
            value = self.dict[key]
            string += (str(key) + ":\n" + value + "\n")
        return string


class LobbyWaitView(generic.FormView):
    """
    View that shows all players in the lobby and their current points
    Calls lobby-wait.html
    """
    template_name = "lobby-wait.html"
    form_class = AddCategoryForm

    def get_context_data(self, **kwargs):
        """
        Aquires all necessary information about the game and the players
        :return: information inside of context var
        """
        game = GameSession.objects.get(lobby_id=self.kwargs["gameid"])
        rounds = list(Round.objects.filter(game=game))
        player_answers_per_round = []
        if rounds:
            running_rounds = list(r for r in rounds if r.started and not r.finished)
            if running_rounds:

                player_answers_per_round = PlayerAnswersPerRound.objects.filter(game=game, player=self.request.user, round=running_rounds[0])

        handlers = PlayerResultHandler.objects.filter(game=game)

        context = super(LobbyWaitView, self).get_context_data(**kwargs)
        context["categories"] = list(game.categories)
        context["player_answers_per_round"] = player_answers_per_round
        context["handlers"] = handlers

        if self.has_everyone_voted(game):
            context["everyone_has_voted"] = True

        if self.request.user == game.admin:
            context["user_is_game_admin"] = True

        if game.rounds <= len(game.get_all_rounds()):
            print("Spiel beendet")
            context["game_ended"] = True

        return context

    def has_everyone_voted(self, game):
        """
        Checks if everyone has voted yet
        :return: TRUE if everyone has voted
        :return: FALSE if there are still people in Vote_View
        """
        players = game.get_all_player_handlers()
        for player in players:
            if player.players_state == 0:
                return False
        return True

    def form_valid(self, form):
        """
        Checks if input is of valid form
        :return: Redirect to get_success_url()
        """
        game = GameSession.objects.get(lobby_id=self.kwargs["gameid"])
        game.categories.append(form.cleaned_data.get('new_category_name'))
        game.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("core:lobby.wait", args=[self.kwargs["gameid"]])


class ResultsView(generic.ListView):
    """
    Last View of the Game
    Orders all Player by score and decide the winner
    Calls results.html
    """
    model = PlayerResultHandler
    template_name = "results.html"

    def get_queryset(self):
        """
        Acquires the game Session and all Players within this session
        :return: List of Players ordered by score
        """
        game = get_object_or_404(GameSession, pk=self.kwargs['gameid'])
        self.list = PlayerResultHandler.objects.filter(game=game.lobby_id).order_by("-total_score")
        return self.list

    def get_context_data(self, **kwargs):
        """
        Acquires context data
        :return: Winner inside context var
        """
        winner, ranks = [self.list[0]], [1]
        rank = 1
        for i in range(1,len(self.list)):
            if self.list[i].total_score == self.list[0].total_score:
                winner.append(self.list[i])
            if self.list[i].total_score < self.list[i-1].total_score:
                rank += 1
                ranks.append(rank)
            else:
                ranks.append(rank)

        context = super(ResultsView, self).get_context_data(**kwargs)
        context['winner'] = winner
        context['ranks'] = zip(self.list, ranks)
        context['winner_count'] = len(winner)
        print(winner,ranks)
        return context


class KickView(generic.ListView):
    """
        View to choose which player should be kicked
        Shows Players and their state ("voted", "not voted", "not played yet") in Radio Button Group
        Calls kick-user.html
    """
    template_name = "kick-user.html"

    def get_queryset(self):
        """
        Acquires the game Session and all Players within this session
        :return: List of Players
        """
        game = get_object_or_404(GameSession, pk=self.kwargs['gameid'])
        self.list = PlayerResultHandler.objects.filter(game=game.lobby_id)
        return self.list

    def get_context_data(self, **kwargs):
        """
        Acquires context data
        :return: handler of all players inside context var
        """
        game = GameSession.objects.get(lobby_id=self.kwargs["gameid"])
        handlers = PlayerResultHandler.objects.filter(game=game)

        context = super(KickView, self).get_context_data(**kwargs)
        context["handlers"] = handlers
        return context
