from django.contrib import admin
from core.models import GameSession, PlayerResultHandler, PlayerAnswersPerRound, Round

# Register your models here.


class GameSessionAdmin(admin.ModelAdmin):
    list_display = ("lobby_id", "admin")


class PlayerResultHandlerAdmin(admin.ModelAdmin):
    list_display = ("player", "game", "total_score")


class PlayerAnswerPerRoundAdmin(admin.ModelAdmin):
    list_display = ("player_answers_id", "player", "round", "answer_score")


class RoundAdmin(admin.ModelAdmin):
    list_display = ("round_id", "letter", "game")


admin.site.register(GameSession, GameSessionAdmin)
admin.site.register(PlayerResultHandler, PlayerResultHandlerAdmin)
admin.site.register(PlayerAnswersPerRound, PlayerAnswerPerRoundAdmin)
admin.site.register(Round, RoundAdmin)

