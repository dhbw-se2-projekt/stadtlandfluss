from django.urls import path, include

# from core.views import GameSessionCreateView
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from . import views

"""
Manages the structure of the different urls and their relation to the views
"""
app_name = "core"

handler400 = 'core.views.bad_request'
handler404 = 'core.views.not_found'
handler403 = 'core.views.permission_denied'

urlpatterns = [
    path('', TemplateView.as_view(template_name='welcome.html'), name='welcome'),
    path('join/', views.join_view, name='game.join'),
    path('create/', views.create_view, name='game.create'),
    path('<uuid:gameid>/lobby', login_required(views.LobbyWaitView.as_view()), name='lobby.wait'),
    path('<uuid:round>/play', login_required(views.play_view), name='lobby.play'),
    path('<uuid:gameid>/start_round', views.start_a_round, name='lobby.start_round'),
    path('<uuid:gameid>/kick', views.KickView.as_view(), name='lobby.kick'),
    path('<uuid:gameid>/delete_user', login_required(views.delete_user), name='lobby.delete_user'),
    path("<uuid:pk>/vote", login_required(views.vote_view), name="lobby.vote"),
    path("<uuid:gameid>/results", login_required(views.ResultsView.as_view()), name="lobby.results"),

]

