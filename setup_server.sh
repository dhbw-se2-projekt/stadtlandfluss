rm -rf stadtlandfluss/core/migrations
rm  stadtlandfluss/db.sqlite3
python3 stadtlandfluss/manage.py makemigrations core
python3 stadtlandfluss/manage.py makemigrations
python3 stadtlandfluss/manage.py migrate
python3 stadtlandfluss/manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', '', 'admin')"
python3 stadtlandfluss/manage.py runserver
