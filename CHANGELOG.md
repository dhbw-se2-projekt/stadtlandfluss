# CHANGELOG
## Version 1.0.0

### New Features
 - answers of other players are shown as tooltip text of own answer on vote page
 - multiple updatable divs (updated by refresher.js) are possible e.g. lobby page can be structured cleaner
### Bug fixes
 - error for new players joining a lobby with a running round
 - state of player is updatable now
 - all winners are shown in the result

## Version 1.0.0-dev()

The release 1.0.0 of StadtLandFluss marks a basic version providing the following features:
- Unique lobbies to play with your friends
- Use the webbrowser of your choice
- Unlimited categories addable through game admin
- Managing players through game admin
- Selectable round quantity
- Selectable round length
- Rating your answers from 0 to 20 Points

### !DISCLAIMER!
There are some bugs left that lead to unclear behavior during the game.
They might be fixed in upcoming versions.
