# StadtLandFluss

Web based online multiplayer game "scattergories" (german: "Stadt Land Fluss").
It is playable on [Pythonanywhere](https://stadtlandfluss.pythonanywhere.com).

![StadtLandFluss Welcome Screen](stadtlandfluss/static/stadtlandfluss/stadtlandfluss-welcome.png)

## Setup

### Install
The tool is developed on Python 3.9 and uses the library [Django](https://docs.djangoproject.com/en/4.1/). 
You can install it using the following code:

```shell
## clone via https or ssh
git clone https://gitlab.com/dhbw-se2-projekt/stadtlandfluss.git
## or git clone git@gitlab.com:dhbw-se2-projekt/stadtlandfluss.git

cd stadtlandfluss/
python -m venv env 
source env/bin/activate 
## or source env/Scripts/activate on windows
pip install -e .
```

### Migrate the database

Before accessing the models, you need to migrate the database.

```shell
python manage.py makemigrations core
python manage.py migrate
```

### Start the development server
You can run the server on your localhost via this command:

```shell
python manage.py runserver
```

With this step, the setup is completed. You can play the Game in your browser now.
## Usage
The game is kind of self-explanatory. So here is just a brief idea of what to do. 

### Game Admin
If you start the game, you become game admin. This is an important role of the game.
The admin provides the game id to the other players. They are also in charge of the categories.
They start every round and can kick other players if they like.

### Beginn
To initialize the game, you need to create one by filling the blanks.
You need to configure your player name, how many rounds you wanna play and 
what's the maximum time each round should take.
After that, you can invite others to play by copying the game id or sending them an invitation link.
All that's left are some categories. You can stick to the classic like town, country and river, or you can get creative 
and make up your own categories. Everything is possible, but you should arrange it with the other players.
In this step you are already in the lobby. Here you come after every round to wait for others or arrange some details.
With the button "start a round" you go to the actual game.

### Play
You have to fill in words fitting the category, that start with the given letter at the top of your view.
Your Goal is to be the fastest and the one with the unique answers.
At the Top is also a Countdown, this is the time you have left to fill in your answers. Attention, as soon as 
one player submits their answers, the time is gone.
You then have to vote your answers.

### Vote
Here you see your answers and point buttons. You have to give yourself points for your answers.
- 0: You don't have one or your answer don't count
- 5: You have an answer but another player has the same one
- 10: You have an answer no one has
- 20: You have an answer but no one else has an answer for that category
In order to give yourself the points, it is best to communicate with the other players.
You can also hover over your own answer and see the words from the others.
After everyone has voted, you can start a new round.

### Finish
The game ends after the given amount of rounds. The player with the highest score wins.

## Database model

```plantuml
class GameSession {
  UUID lobby_id PK
  User admin FK
  __
  int rounds
  int round_max_length
  string letters
  JSON categories
  ==
  get_all_player_handlers()
  get_all_rounds()
  get_all_answers()
  get_a_letter()
}

class PlayerResultHandler {
  GameSession game FK
  User player FK
  __
  int total_score
  int players_state
}

class User {
  int id PK
  string username
  ...
  (Klasse wird von Django bereitgestellt)
}

class Round{
  UUID round_id PK
  GameSession game FK
  __
  string letter
  boolean started
  boolean finished
}

class PlayerAnswersPerRound{
  UUID player_answers_id PK
  User player FK
  GameSession game FK
  Round round FK
  __
  JSON answers
  int answer_score
}

User "1" -up- "1" PlayerResultHandler
User "1" -ri- "m" PlayerAnswersPerRound
GameSession "1" -- "m" Round
PlayerAnswersPerRound "m" -ri- "1" Round
PlayerAnswersPerRound "m" -up- "1" GameSession
GameSession "1" -le- "n" PlayerResultHandler
```

## Some Comments
- For testing reasons you have to use different browsers to imitate different player.
You can also use incognito-browser.
- The Issues in this project are some additions, one could make in the future.


## Contributors

- Benjamin Bauer
- Jörn Vaupel
- Konstantin Eisebitt
- Simon Spielmann
- Sophie Kernchen

## Changelog
Please see [CHANGELOG.md](CHANGELOG.md) for further information.

## License
This project is currently not published under any license.
